$url = "https://static.rust-lang.org/rustup/dist/x86_64-pc-windows-msvc/rustup-init.exe"
$output = "$HOME\rustup-init.exe"
$start_time = Get-Date

Invoke-WebRequest -Uri $url -OutFile $output
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

. $HOME\rustup-init.exe

code --install-extension ms-vscode.cpptools
code --install-extension rust-lang.rust
